// Wait for the DOM to be ready
$(function(){
	
	jQuery.validator.addMethod(
			"regex",
			function(value, element, regexp) {
			if (regexp.constructor != RegExp)
			regexp = new RegExp(regexp);
			else if (regexp.global)
			regexp.lastIndex = 0;
			return this.optional(element) || regexp.test(value);
			},"erreur expression reguliere"
			);
	$.validator.addMethod('filesize', function (value, element, param) {
	    return this.optional(element) || (element.files[0].size <= param * 1000000)
	}, 'File size must be less than {0} MB');

	console.log(portletNameSpace);
	var firstName = portletNameSpace + 'firstName';
	var lastName = portletNameSpace + 'lastName';
	var emailAddress = portletNameSpace + 'emailAddress';
	var studentId = portletNameSpace + 'studentId';
	var formCheck = portletNameSpace + 'formCheck';
  $("form[name='userForm']").validate({
    rules: {
    	[firstName]: {
    		required : true,
    		regex: "^[a-zA-Z]"
    	},
    	[lastName]: {
    		required : true,
    		regex: "^[a-zA-Z]"
    	},
    	[emailAddress]: {
        required: true, 
        email: true
      },
      [emailAddress]: {
          required: true,
          email: true
        },
        [studentId]: {
            required: true,
            regex : "^0[0-9].[0-9]{0,6}"
          },
     [formCheck]:{
    	   required: true,
       },
          
    },
    messages: {
    	[firstName]: "Please enter valid firstname",
    	[lastName]: "Please enter valid lastname",
    	[emailAddress]: "Please enter a valid email address",
    	[studentId] : "Please enter a valid Student Id",
    	[formCheck] :  "click on"
    },
    submitHandler: function(form) {
    	
    	
      form.submit();
    }
    
  });
});

$(document).ready(function(){

	$('.customFileUpload').each(function() {
	      $(this).rules('add', {
	          required: true,
	          filesize: 15,
	          messages: {
	              required:  "File upload is required",
	            	  filesize: "Uploaded file must be less than 15mb "  
	          }
	      });
	  });
	
	$("#documentType").on('change', function () {
		if($("#documentType").val() == '0') {
			$("#uploadfile1").attr('disabled' , 'true' );
			$("#btnAddDocument").attr('disabled' , 'true' );
			
		}
		else
			{
				$("#uploadfile1").removeAttr('disabled' , 'false' );
				$("#btnAddDocument").removeAttr('disabled' , 'false' );
				$("#uploadfile1").attr("accept",$('#documentType').val());
			}
	});
});


$(function () {
	var fileUploadCount = $("#fileUploadCount");
    $("#btnAddDocument").bind("click", function () {
    	
    	fileUploadCount.val(parseInt(fileUploadCount.val()) + 1);
    	$("#documentType").attr('disabled' , 'true' );
    	var documentType =   $("#documentType").val();
    	var div = $("<div class='form-group' id='addDiv"+fileUploadCount.val()+"' />");
        div.html(GetDynamicFileUploadTextBox(fileUploadCount,documentType));
        $("#fileUploadBoxContainer").append(div);
        
        $('.customFileUpload').each(function() {
  	      $(this).rules('add', {
  	          required: true,
  	          filesize: 15,
  	          messages: {
  	              required:  "File upload is required",
  	            	  filesize: "Uploaded file must be less than 15mb "  
  	          }
  	      });
  	  });
   });
    $("body").on("click", ".remove", function () {
        $(this).closest("div").remove();
        if($(".customFileUpload").length == 1){
        	$("#documentType").removeAttr('disabled' , 'false' );
        	}
    	 
    });

    $("form").on('change', function () {
    	var ids = $('.customFileUpload').map(function() {
    		  return $(this).attr('id');
    		}).get();
    		console.log(ids);
    		 $("#fileUploadedId").attr("value",ids);
    	});
});
function GetDynamicFileUploadTextBox(value,documentType) {
    return '<input type="file" id="uploadfile'+ value.val() +'" class="customFileUpload" accept="'+documentType+'" name="'+portletNameSpace+'uploadfile'+ value.val() +'">&nbsp;' +
            '<input type="button" value="Remove" class="remove" />'
}



