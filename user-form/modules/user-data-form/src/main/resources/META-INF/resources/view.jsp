<%@ include file="/init.jsp" %>
<portlet:actionURL name="/user-form" var="FormAction"/>

<div><script type="text/javascript">
var portletNameSpace = '<portlet:namespace/>'</script></div>
<div>
<form action="${FormAction}" method="post" enctype="multipart/form-data" name="userForm" id="userForm">
		<label class="lb-hader">APPLICANT INFORMATION</label>
		<hr class="line"/>
			<div class="card-body  form-controller">
				<div class ="row" >
					<div class="col-md-6">
						<label>FIRST NAME</label><br>
						<input class="form-control" value="" name="<portlet:namespace/>firstName" id="firstName" />
					</div>
					<div class="col-md-6">
						<label>LAST NAME</label><br>
						<input  class="form-control" value="" name="<portlet:namespace/>lastName" id="lastName" />
					</div>
				</div>
				<div>
					<label>EMAIL ADDRESS</label><br>
					<input class="form-control" value="" name="<portlet:namespace/>emailAddress" id="emailAddress" />
				</div>
				<div>
					<label>STUDENT ID</label><br>
					<input class="form-control" value="" type="number" name="<portlet:namespace/>studentId" id="studentId" minlength=""8 maxlength="8"/>
					<p class="lb-hader">ENTER ONLY ONE LEADING ZERO FOR YOUR STUDENT ID NUMBER (EX:01234567)</p>
				</div>
			</div>
		
		<label class="lb-hader">DOCUMENT INFORMATION AND UPLOAD</label>
		<hr class="line"/>
			<div class="card-body">
					<p>
						PLEASE SELACT THE CORRESPONDING DOCUMENT TYPE BLOW BEFORE ATTACHING THE FILE. DONOT SUBMIT PASSWORD 
						PROTECTED FILES OR FILES OVER 15MB. ACCEPTABLE FILE FORMATE: .DOC, .DOCX, .XLS, .XLSX, .JPG, OR .PDF
					</p>
					<div class="col-sm-6">
						<div class="form-group">
						<label>DOCUMENT TYPE</label>
							<select  class ="form-control custom-select" name="<portlet:namespace/>documentType"  id="documentType" >
							  <option value="0">SELECT OPTION</option>
							  <option value="application/.doc">.DOC</option>
							  <option value="application/.docx">.DOCX</option>
							  <option value="application/.xls">.XLS</option>
							  <option value="application/excel">.XLSX</option>
							  <option value="image/jpeg">.JPG</option>
							  <option value="application/pdf">.PDF</option>
							</select>
							
							</div>
								<div class="form-group">
									<input type="hidden" value="1" id="fileUploadCount" name="<portlet:namespace/>fileUploadCount">
									<input type="hidden" value="" id="fileUploadedId" name="<portlet:namespace/>fileUploadedId">
									
									<input type="file" class="customFileUpload" id="uploadfile1" name="<portlet:namespace/>uploadfile1" disabled>
								</div>
								<div id="fileUploadBoxContainer"  class="form-group">
    								<!--Textboxes will be added here -->
								</div>
								<div class="form-group">		
									<button name="btnAddDocument"  id="btnAddDocument" type="button" disabled>ADD MORE DOCUMENTS</button>
								</div>
					</div>
					
			</div>
		<label class="lb-hader">TERMS AND CONDITIONS</label>
		<hr class="line"/>
			<div class="card-body">
				<div class="container-fluid">
					<div class=" row">
					<div class="col-1 text-center">
						<input class="form-check-input" type="checkbox" name="<portlet:namespace/>formCheck" value="formCheck"/>
					</div>
					<div class="col-11">
					<p class="form-check-label">
							BY SUBMITTING THIS FINANCIAL AID APPLICATION, I HEREBY CONSENTS TO STANFORD UNIVERSITY'S COLLECTION 
							AND PROCESSING OF ANY SENSITIVE PERSONAL DATA CONTAINED INMY APPLICATION TO EVALUATE MY APPLICATION 
							FOR THE OTHER PURPOSES DESCRIBED IN STANFORDUNIVERSITY'S ONLINE PRIVACY, OFFLINE PRIVACY POLICY, AND 
							THE PRIVACY NOTICE FOR ADMISSIONS AND FINACIAL AID WHICH CAN BE FOUND AT PRIVACY.STANFORD.EDU		
						</p>
					</div>
						
						
					</div>
				</div>
				<div class ="sheet-footer pull-right">
					<button class="btn  btn-cancel btn-default btn-link" type="reset" id="btnFormCancle">CANCLE</button>
					<button class="btn btn-primary" type="submit" id="btnFormSubmit">SUBMIT</button>
				</div>
			</div>
</form>
</div>



