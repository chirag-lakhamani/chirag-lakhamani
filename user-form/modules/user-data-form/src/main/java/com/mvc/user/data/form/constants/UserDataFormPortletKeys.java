package com.mvc.user.data.form.constants;

/**
 * @author ignek
 */
public class UserDataFormPortletKeys {

	public static final String USER_DATA_FORM =
		"com_mvc_user_data_form_UserDataFormPortlet";

}