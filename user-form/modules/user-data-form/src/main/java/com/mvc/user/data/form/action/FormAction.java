package com.mvc.user.data.form.action;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.model.DLFolder;
import com.liferay.document.library.kernel.model.DLFolderConstants;
import com.liferay.document.library.kernel.service.DLAppService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.security.auth.PrincipalThreadLocal;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.security.permission.PermissionCheckerFactoryUtil;
import com.liferay.portal.kernel.security.permission.PermissionThreadLocal;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.ListTypeLocalService;
import com.liferay.portal.kernel.service.PhoneLocalService;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.UserGroupLocalService;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.upload.FileItem;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Time;
import com.liferay.portal.kernel.util.WebKeys;
import com.mvc.user.data.form.constants.UserDataFormPortletKeys;

/**
 * @author ignek
 */
@Component(
    immediate = true,
    property = {
        "javax.portlet.name=" + UserDataFormPortletKeys.USER_DATA_FORM,
        "mvc.command.name=/user-form"
    },
    service = MVCActionCommand.class
)
public class FormAction extends BaseMVCActionCommand{

	private static final Log log = LogFactoryUtil.getLog(FormAction.class.getName());
	private static String ROOT_FOLDER_NAME = "DOCUMENTS";//PropsUtil.get("fileupload.folder.name");
	private static String ROOT_FOLDER_DESCRIPTION = "UPLOADED DOCUMENTS";//PropsUtil.get("fileupload.folder.description");
	private static long PARENT_FOLDER_ID = DLFolderConstants.DEFAULT_PARENT_FOLDER_ID;
	
	@Reference
	private Portal _portal;

	@Reference
	UserLocalService userLocalService;

	@Reference
	DLAppService dLAppService;

	@Reference
	RoleLocalService roleLocalService;
	
	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		
		log.info("-- Form Data --");
		String firstName = ParamUtil.getString(actionRequest, "firstName", null);
		log.info("firstName -->" + firstName);
		String lastName = ParamUtil.getString(actionRequest, "lastName", null);
		log.info("lastName -->" + lastName);
		String emailAddress = ParamUtil.getString(actionRequest, "emailAddress", null);
		log.info("emailAddress -->" + emailAddress);
		String studentId = ParamUtil.getString(actionRequest, "studentId", null);
		log.info("studentId -->" + studentId);
		String documentType = ParamUtil.getString(actionRequest, "documentType", null);
		log.info("documentType -->" + documentType);
		String formCheck = ParamUtil.getString(actionRequest, "formCheck", null);
		log.info("formCheck -->" + formCheck);
		
		
//		file upload code
		createFolder(actionRequest, themeDisplay);
		ArrayList<String> fileNameList = fileUpload(themeDisplay, actionRequest);
		
		//String fileNameLLIst = ParamUtil.getString(actionRequest, "uploadfile1", null);
		for (String fileName : fileNameList) {
			log.info("uploadfile -->" + fileName);
		}
		//log.info("uploadfile -->" + uploadfile);
		
		
	}
	
	public Folder createFolder(ActionRequest actionRequest, ThemeDisplay themeDisplay) {
		boolean folderExist = isFolderExist(themeDisplay);
		Folder folder = null;
		if (!folderExist) {
			long repositoryId = themeDisplay.getScopeGroupId();
			try {
				ServiceContext serviceContext = ServiceContextFactory.getInstance(DLFolder.class.getName(),
						actionRequest);

				folder = dLAppService.addFolder(repositoryId, PARENT_FOLDER_ID, ROOT_FOLDER_NAME,
						ROOT_FOLDER_DESCRIPTION, serviceContext);

			} catch (PortalException e) {
			} catch (Exception e) {
			}
		}
		return folder;
	}
	
	public boolean isFolderExist(ThemeDisplay themeDisplay) {
		boolean folderExist = false;
		try {
			dLAppService.getFolder(themeDisplay.getScopeGroupId(), PARENT_FOLDER_ID, ROOT_FOLDER_NAME);
			folderExist = true;
		} catch (Exception e) {
		}
		return folderExist;
	}

	public Folder getFolder(ThemeDisplay themeDisplay) {
		Folder folder = null;
		try {
			folder = dLAppService.getFolder(themeDisplay.getScopeGroupId(), PARENT_FOLDER_ID, ROOT_FOLDER_NAME);
		} catch (Exception e) {
		}
		return folder;
	}

	public ArrayList<String> fileUpload(ThemeDisplay themeDisplay, ActionRequest actionRequest) {
		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		ArrayList<String> fileName =new  ArrayList<String>();
		Map<String, FileItem[]> files = uploadPortletRequest.getMultipartParameterMap();
		Folder folder = getFolder(themeDisplay);
		InputStream imputStream;
		String title, description, mimeType;
		long repositoryId;
		for (Entry<String, FileItem[]> file : files.entrySet()) {

			FileItem item[] = file.getValue();

			try {
				for (FileItem fileItem : item) {
					title = (new Date().getTime()) +"-"+ fileItem.getFileName();
					fileName.add(fileItem.getFileName());
					description = title;
					repositoryId = themeDisplay.getScopeGroupId();
					mimeType = fileItem.getContentType();
					imputStream = fileItem.getInputStream();
					try {
						ServiceContext serviceContext = ServiceContextFactory.getInstance(DLFileEntry.class.getName(),
								actionRequest);
						
						dLAppService.addFileEntry(repositoryId, folder.getFolderId(), title, mimeType, title,
								description, GetterUtil.DEFAULT_STRING, imputStream, fileItem.getSize(), serviceContext);
						//log.info("file uploaded");
						
					} catch (PortalException e) {
						log.info(e);
					} catch (SystemException e) {
						log.info(e);
					}
				}
			} catch (IOException e) {
				log.error(e);
			}
		}
		return fileName;
	}
}
