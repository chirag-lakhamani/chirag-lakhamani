package com.mvc.user.data.form.portlet;

import com.mvc.user.data.form.constants.UserDataFormPortletKeys;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

/**
 * @author ignek
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.header-portlet-css=/css/Form.css",
		"com.liferay.portlet.header-portlet-javascript=/js/form-validation.js",
		"com.liferay.portlet.header-portlet-javascript=/js/jquery.min.js",
		"com.liferay.portlet.header-portlet-javascript=/js/jquery.validate.min.js",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=UserDataForm",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + UserDataFormPortletKeys.USER_DATA_FORM,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class UserDataFormPortlet extends MVCPortlet {
}